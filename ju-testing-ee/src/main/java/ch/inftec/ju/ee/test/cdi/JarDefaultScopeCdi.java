package ch.inftec.ju.ee.test.cdi;

/**
 * POJO in a non-beans enabled JAR.
 * @author Martin Meyer <martin.meyer@inftec.ch>
 *
 */
public class JarDefaultScopeCdi {
}

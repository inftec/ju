package ch.inftec.ju.maven.test;

import ch.inftec.ju.util.JuException;
import ch.inftec.ju.util.JuUrl;
import ch.inftec.ju.util.xml.XPathGetter;
import ch.inftec.ju.util.xml.XmlUtils;
import org.apache.maven.plugin.Mojo;
import org.apache.maven.plugin.testing.AbstractMojoTestCase;
import org.codehaus.plexus.configuration.PlexusConfiguration;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

/**
 * Created by rotscher on 13.11.15.
 */
public class VersionOverrideMojo extends AbstractMojoTestCase {


    @Override
    protected Mojo lookupMojo(String groupId, String artifactId, String version, String goal, PlexusConfiguration pluginConfiguration) throws Exception {

        //in case version override is enabled
        String versionOverride = System.getProperty("version.override");
        if (versionOverride != null) {
            String overriddenVersion = readOverriddenVersion();
            return super.lookupMojo(groupId, artifactId, overriddenVersion, goal, pluginConfiguration);
        }

        return super.lookupMojo(groupId, artifactId, version, goal, pluginConfiguration);
    }

    private String readOverriddenVersion() throws IOException, JuException {
        //make sure this file is written before tests are executed, see ch.rotscher.maven.plugins:install-custom-version-plugin
        String version = new XPathGetter(XmlUtils.loadXml(JuUrl.toUrl(JuUrl.existingFile("pom-version.override.xml")))).getSingle("project/parent/version");
        return version;
    }
}

package ch.inftec.ju.versioverride;

import java.io.File;
import java.io.IOException;

/**
 * enum of different build server products
 */
public enum BuildServerType {

    BAMBOO(new BambooBuildServer()),
    JENKINS(new JenkinsBuildServer()),
    UNKNOWN(new LocalBuildMachine());

    private BuildServer classifierBuilder;

    BuildServerType(BuildServer classifierBuilder) {
        this.classifierBuilder = classifierBuilder;
    }

    public BuildServer getBuildServer() {
        return this.classifierBuilder;
    }

    /**
     * jenkins build server
     */
    static class JenkinsBuildServer extends AbstractBuildServer {

        @Override
        public String getBranchName() {
            return System.getenv("GIT_BRANCH");
        }

        @Override
        public String getJobName() {
            return System.getenv("JOB_NAME");
        }

        @Override
        public String getProductName() {
            return "jenkins";
        }

        @Override
        public String getBuildNumber() throws IOException {
            String jenkinsBuildNumber = System.getenv("BUILD_NUMBER");
            return jenkinsBuildNumber;
        }
    }

    /**
     * bamboo build server
     */
    static class BambooBuildServer extends AbstractBuildServer {

        @Override
        public String getBranchName() {
            return System.getenv("bamboo_repository_branch_name");
        }

        @Override
        public String getJobName() {
            return System.getenv("bamboo_buildKey");
        }

        @Override
        public String getProductName() {
            return "bamboo";
        }

        @Override
        public String getBuildNumber() throws IOException {
            String bambooBuildNumber = System.getenv("bamboo_buildNumber");
            return bambooBuildNumber;
        }
    }

    /**
     * the local build machine
     */
    static class LocalBuildMachine extends AbstractBuildServer {

        @Override
        public String getBranchName() {
            return "local";
        }

        @Override
        public String getJobName() {
            return "local";
        }

        @Override
        public String getProductName() {
            return "local";
        }

        @Override
        public String getBuildNumber() throws IOException {
            return readFromFile();
        }

        private String readFromFile() throws IOException {
            String buildNumber = "0";
            File buildNumberFile = new File(VersionOverrideModelReader.MAVENEXT_BUILDNUMBER_FILE);
            if (buildNumberFile.exists()) {
                buildNumber = BuildnumberFile.readFirstLine(buildNumberFile);
                buildNumberFile.delete();
            }
            int incrementedBuildNumber = Integer.parseInt(buildNumber) + 1;
            buildNumber = incrementedBuildNumber + "";
            BuildnumberFile.writeLine(buildNumber, buildNumberFile);
            /*logger.info(String
                    .format("using buildnumber from file .buildnumber: %s",
                            buildNumber));*/
            return buildNumber;
        }
    }
}

package ch.inftec.ju.versioverride;

import org.apache.maven.model.Model;
import org.codehaus.plexus.logging.Logger;

import java.io.IOException;


/**
 * factory for creating and caching the version strategy for a build
 */
public class VersionFactory {

    public static final String PROPERTY_BUILDID = "version.buildId";

    /* package private for unit test cleanup */
    static Version version;


    /**
     * create and return the version strategy, which is either a static value in case the version is given as
     * system property (-Dversion.override=1.0)  or it's a generated version in case the version override feature is
     * enabled (-Dversion.override) but no static value is given.
     *
     * @param overriddenVersion a static version or an empty string
     * @param model the maven model
     * @param logger a maven logger
     * @return a version instance for handling overridden version
     * @throws IOException
     */
    public static Version create(String overriddenVersion, Model model, Logger logger) throws IOException {

        BuildServer buildServer = AbstractBuildServer.detectBuildServer(logger).getBuildServer();
        return create(overriddenVersion, model, buildServer, logger);
    }

    /* package private for unit testing */
    static Version create(String overriddenVersion, Model model, BuildServer buildServer, Logger logger) throws IOException {

        if (version == null) {
            version = VersionStrategy.determine(overriddenVersion).create(model, overriddenVersion, buildServer, logger);
            logger.info(String.format("version for version.override is %s", version.getOverriddenVersion()));

        }

        return version;
    }
}

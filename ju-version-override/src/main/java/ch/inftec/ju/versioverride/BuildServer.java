package ch.inftec.ju.versioverride;
//

import java.io.IOException;

/**
 * interface for different build/continuous integration servers like jenkins, bamboo etc
 */
public interface BuildServer {

    /**
     * get the branch name of the current build. Normally, the build server injects this value as environment variable
     * @return the branch name
     */
    String getBranchName();

    /**
     * get the job name of the current build. Normally, the build server injects this value as environment variable
     * @return the job name
     */
    String getJobName();

    /**
     * get the product name of the build server
     * @return the product name
     */
    String getProductName();

    /**
     * get the buildnumber, the buildnumber generation is dependent of the build server
     *
     * @return the buildnumber
     */
    String getBuildNumber() throws IOException;

    boolean isOnMasterBranch();
    boolean isOnDevelopBranch();
}

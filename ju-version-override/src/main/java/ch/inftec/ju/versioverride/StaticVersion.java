package ch.inftec.ju.versioverride;

import org.apache.maven.model.Model;
import org.codehaus.plexus.logging.Logger;

import java.io.IOException;

/**
 * a static version is given from outside (-Dversion.override=1.2.3-rc-1). The only generated value is the buildId
 * which can be enabled or disabled with <code>-Dversion.buildId=[true|false]</code>
 * Created by rotscher on 1/15/15.
 */
public class StaticVersion implements Version {


    private final String groupId;
    private final String originalVersion;
    private final String version;

    public StaticVersion(Model model, String overriddenVersion, BuildServer buildServer, Logger logger) throws IOException {
        this.groupId = VersionOverrideModelReader.getGroupId(model);
        this.originalVersion = model.getVersion();
        this.version = generateVersion(overriddenVersion, buildServer, logger);

        logger.info(String
                .format("initialize groupId to '%s', the version %s will be applied to all modules and dependencies " +
                                "starting with this groupdId will be changed!",
                        groupId, this.version));

    }

    private String generateVersion(String overriddenVersion, BuildServer buildServer, Logger logger) {
        boolean withBuildId = Boolean.valueOf(System.getProperty(VersionFactory.PROPERTY_BUILDID, "true"));
        if (withBuildId) {
            String buildNumber = getBuildNumber(buildServer, logger);
            logger.info("appending the buildId to the overridden version.");
            return String.format("%s-%s", overriddenVersion, buildNumber);
        } else {
            return overriddenVersion;
        }
    }

    private String getBuildNumber(BuildServer buildServer, Logger logger) {
        try {
            return buildServer.getBuildNumber();
        } catch (IOException e) {
            logger.warn(e.getMessage(), e);
        }

        return "nA";
    }


    @Override
    public String getOriginalVersion() {
        return originalVersion;
    }

    @Override
    public String getGroupId() {
        return groupId;
    }

    @Override
    public String getOverriddenVersion() {
        return version;
    }
}

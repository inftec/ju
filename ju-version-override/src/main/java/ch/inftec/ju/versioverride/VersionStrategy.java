package ch.inftec.ju.versioverride;

import org.apache.maven.model.Model;
import org.codehaus.plexus.logging.Logger;

import java.io.IOException;

/**
 * specify different strategies to generate a version
 * Created by rotscher on 17.09.15.
 */
public enum VersionStrategy {

    /* the version is provided from external, ${project.version} from pom.xml is not considered */
    EXTERNAL {
        @Override
        public Version create(Model model, String overriddenVersion, BuildServer buildServer, Logger logger) throws IOException {
            return new StaticVersion(model, overriddenVersion, buildServer, logger);
        }
    },

    /* the main version is taken from pom.xml, classifier is either determined on branch or other development information */
    POM {
        @Override
        public Version create(Model model, String overriddenVersion, BuildServer buildServer, Logger logger) throws IOException {
            return new GeneratedVersion(model, buildServer, logger);
        }
    },
    /* this "strategy" is used in case version.override is set to false */
    NONE {
        @Override
        public Version create(Model model, String overriddenVersion, BuildServer buildServer, Logger logger) throws IOException {
            return new PomVersion(model, logger);
        }
    };

    public static VersionStrategy determine(String value) {
        if (value != null && value.trim().length() > 0) {
            if (value.toLowerCase().equals("true")) {
                return POM;
            } else if (value.toLowerCase().equals("false")) {
                return NONE;
            } else {
                //version is set from outside
                return EXTERNAL;
            }
        }

        //version override is active with no value set....
        return POM;
    }

    /**
     * create a version instance populated with the given parameters
     *
     * @param model
     * @param overriddenVersion
     * @param buildServer
     * @param logger
     * @return an instance of version
     * @throws IOException
     */
    public abstract Version create(Model model, String overriddenVersion, BuildServer buildServer, Logger logger) throws IOException;
}

package ch.inftec.ju.versioverride;

/**
 * Created by rotscher on 1/15/15.
 */
public interface Version {

    String getOriginalVersion();
    String getGroupId();
    String getOverriddenVersion();

}

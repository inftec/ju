package ch.inftec.ju.versioverride;

import org.apache.maven.model.Model;
import org.codehaus.plexus.logging.Logger;

import java.io.IOException;

/**
 * just use the pom version, the version is not modified
 *
 * Created by rotscher on 1/15/15.
 */
public class PomVersion implements Version {


    private final String groupId;
    private final String originalVersion;
    private final String version;

    public PomVersion(Model model, Logger logger) throws IOException {
        this.groupId = VersionOverrideModelReader.getGroupId(model);
        this.originalVersion = model.getVersion();
        this.version = this.originalVersion;

        logger.info(String
                .format("version.override is active, but the version is not overridden, version %s from pom.xml is used",
                        this.version));

    }

    @Override
    public String getOriginalVersion() {
        return originalVersion;
    }

    @Override
    public String getGroupId() {
        return groupId;
    }

    @Override
    public String getOverriddenVersion() {
        return version;
    }
}

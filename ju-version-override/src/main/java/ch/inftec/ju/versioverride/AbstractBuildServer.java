package ch.inftec.ju.versioverride;

import org.codehaus.plexus.logging.Logger;

/**
 * Created by rotscher on 3/12/15.
 */
public abstract class AbstractBuildServer implements BuildServer {

    static BuildServerType buildServerType;

    @Override
    public boolean isOnMasterBranch() {
        String branch = getBranchName();
        if (branch != null && branch.contains("master")) {
            return true;
        }
        return false;
    }

    @Override
    public boolean isOnDevelopBranch() {
        String branch = getBranchName();
        if (branch != null && branch.contains("develop")) {
            return true;
        }
        return false;
    }

    /**
     * detect build server. This is done in an automatic way with finding product specific environment variables
     * or in a manual way, when system property <code>buildserver.type</code> is set.
     *
     * @param logger the logger
     * @return a build server type
     */
    public static BuildServerType detectBuildServer(Logger logger) {

        if (buildServerType == null) {
            String buildServerProperty = System.getProperty("buildserver.type");
            if (buildServerProperty != null && buildServerProperty.trim().length() > 0) {
                buildServerType = BuildServerType.valueOf(buildServerProperty);
                logger.info(String
                        .format("using buildserver '%s', which was set with -Dbuildserver.type=%s",
                                buildServerType.getBuildServer().getProductName(), buildServerProperty));
                return buildServerType;
            }

            /* used to print build server information */
            String buildServerEnvironment = "";

            if (System.getenv("JOB_NAME") != null && !System.getenv("JOB_NAME").equals("null")) {
                buildServerType = BuildServerType.JENKINS;
                buildServerEnvironment = String.format(" (based on environment 'JOB_NAME' which was set to '%s)'", System.getenv("JOB_NAME"));
            } else if (System.getenv("bamboo_buildKey") != null && !System.getenv("bamboo_buildKey").equals("null")) {
                buildServerType = BuildServerType.BAMBOO;
                buildServerEnvironment = String.format(" (based on environment 'bamboo_buildKey' which was set to '%s)'", System.getenv("bamboo_buildKey"));
            } else {
                buildServerType =  BuildServerType.UNKNOWN;
            }
            logger.info(String
                    .format("detected buildserver '%s'%s, " +
                                    "override this with setting -Dbuildserver.type=JENKINS|BAMBOO|UNKNOWN",
                            buildServerType.getBuildServer().getProductName(), buildServerEnvironment));
        }
        return buildServerType;

    }
}

package ch.inftec.ju.versioverride;

import org.apache.maven.model.Model;
import org.codehaus.plexus.logging.Logger;

import java.io.IOException;

/**
 * Version is generated based on ${project.version} and either based on
 * an issue id (branch pattern: origin/feature/PR-15_myfeature) or the branch (branch pattern: origin/beta/1.2)
 *
 * Created by rotscher on 1/15/15.
 */
public class GeneratedVersion implements Version {
    private final String groupId;
    private final String originalVersion;
    private final String version;
    private final Logger logger;

    public GeneratedVersion(Model model, BuildServer buildServer, Logger logger) throws IOException {
        this.logger = logger;
        this.groupId = VersionOverrideModelReader.getGroupId(model);
        this.originalVersion = model.getVersion();
        this.version = generateVersion(model, buildServer);
        //System.setProperty(MAVENEXT_RELEASE_VERSION, this.version);

        logger.info(String
                .format("initialize groupId to '%s', the version of all modules and dependencies starting with this groupdId will be changed!",
                        groupId));
    }

    private String generateVersion(Model model, BuildServer buildServer) {
        //generate a version based on the {project.version}
        String[] pomVersionTokens = model.getVersion().split("-");
        String buildId = "nA";
        try {
            buildId = buildServer.getBuildNumber();
        } catch (IOException e) {
            logger.warn(e.getMessage(), e);
        }

        if (buildServer.isOnMasterBranch()) {
            logger.info(String.format("branch is 'master', only append the buildId to the version in the pom, classifier is ignored"));
            return String.format("%s-%s", pomVersionTokens[0], buildId);
        } else if (buildServer.isOnDevelopBranch()) {
            logger.info(String.format("branch is 'develop', take the snapshot version and append a buildId"));
            //we always set the classifier to S which is the short form of SNAPSHOT
            return String.format("%s-%s-%s", pomVersionTokens[0], "S", buildId);
        } else {
            logger.info(String.format("branch is %s, generate a version", buildServer.getBranchName()));
            VersionClassifier versionClassifier = new VersionClassifier(logger, buildServer);
            String classifier = versionClassifier.getClassifier(pomVersionTokens[1]);
            return String.format("%s-%s-%s", pomVersionTokens[0], classifier, buildId);
        }
    }

    @Override
    public String getOriginalVersion() {
        return originalVersion;
    }

    @Override
    public String getGroupId() {
        return groupId;
    }

    @Override
    public String getOverriddenVersion() {
        return version;
    }
}

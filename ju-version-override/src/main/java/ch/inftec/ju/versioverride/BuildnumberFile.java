package ch.inftec.ju.versioverride;

import java.io.*;

/**
 * helper class for write and read a number from a file
 */
public class BuildnumberFile {

    public static String readFirstLine(File file) throws IOException {

        BufferedReader br = null;

        try {
            br = new BufferedReader(new FileReader(file));
            return br.readLine();
        } finally {
            if (br != null)
                br.close();
        }
    }

    public static void writeLine(String line, File file) throws IOException {
        if (!file.exists()) {
            file.createNewFile();
        }


        FileOutputStream fos = new FileOutputStream(file);

        // get the content in bytes
        byte[] contentInBytes = line.getBytes();
        fos.write(contentInBytes);
        fos.flush();
        fos.close();
    }
}

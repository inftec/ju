package ch.inftec.ju.versioverride;

import org.codehaus.plexus.logging.Logger;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * class to determine the classifier for a generated version
 *
 * Created by rotscher on 1/22/15.
 */
public class VersionClassifier {

    private final BuildServer buildServer;

    enum ClassifierType {
		RELEASE, DEVELOP, PERSONAL
    }

    private static String branchPattern = "[A-Z]+-[0-9]+";
    private static String releasePattern = "(.*/)*(.*)(/.*)";
    private Logger logger;

    public VersionClassifier(Logger logger, BuildServer buildServer) {
        this.logger = logger;
        this.buildServer = buildServer;
    }

    /**
     * determine the classifier. Determination is depended on the classifier type, namely if we create a
     * development or a release build. The default classifier "SNAPSHOT" is shorten to "S".
     *
     * @param defaultClassifier default classifier is the one to be used when no classifier can be determined
     * @return the generated classifier
     */
    public String getClassifier(String defaultClassifier) {
		ClassifierType type = ClassifierType.valueOf(System.getProperty("classifier.type", "DEVELOP"));

        logger.info(String
                .format("using classifier.type '%s', override this with setting -Dclassifier.type=RELEASE|DEVELOP|PERSONAL",
                        type));

        if (type == ClassifierType.PERSONAL) {

            String issueIdentifier = getIssueIdentifierFromBranch(buildServer.getBranchName());
            String jobName = buildServer.getJobName();

            if (issueIdentifier != null) {
                logger.info(String
                        .format("found an issue id in branch name, personal classifier is: %s",
                                issueIdentifier));
                return String.format("%s_%s", jobName, issueIdentifier);
            }

            logger.info(String
                    .format("no issue id found in branch name, personal classifier is: %s",
                            issueIdentifier));
            logger.info(String
                    .format("additional info: naming pattern of your branch for having an issue id must be " +
                            "something like this 'feature/ABC-765_some-fancy-branch-name'"));
            return String.format("%s", jobName);
        } else if (type == ClassifierType.DEVELOP) {
            String issueIdentifier = getIssueIdentifierFromBranch(buildServer.getBranchName());
            if (issueIdentifier != null) {
                logger.info(String
                        .format("found an issue id in branch name, classifier is: %s",
                                issueIdentifier));
                return String.format("%s", issueIdentifier);
            }

            logger.info(String
                    .format("no issue id found in branch name, classifier is: %s",
                            defaultClassifier));
            logger.info(String
                    .format("additional info: naming pattern of your branch for having an issue id must be " +
                            "something like this 'feature/ABC-765_some-fancy-branch-name'"));
        } else if (type == ClassifierType.RELEASE) {
            String classifier = getReleaseIdentifierFromBranch(buildServer.getBranchName());
            if (classifier != null) {
                return classifier;
            }

            logger.warn(String
                    .format("despite classifier.type is set to '%s' no branch label has been found (e.g. origin/beta/PR-11_myfeaturebranch)",
                            type));
		}

        if (defaultClassifier.equals("SNAPSHOT")) {
            //we shorten a little and avoid the literal 'SNAPSHOT'
            //this is the classifier on 'develop' branches
            defaultClassifier = "S";
        }

        logger.info(String
                .format("using classifier '%s' from pom.xml as is (except 'SNAPSHOT' which gets shorten to 'S')",
                        defaultClassifier));

        return defaultClassifier;
    }

    /**
     * find the issue identifier of pattern "PR-6" or "JIRA-8" in the given input (e.g. origin/feature/JIRA-8_myfeature)
     *
     * @param input the input string to parse (normally the branch name)
     * @return the issue identifier, or null if nothing can be found
     */
    public String getIssueIdentifierFromBranch(String input) {

        if (input == null) {
            return null;
        }

        //find something like PR-5, TEC-7
        Pattern pattern = Pattern.compile(branchPattern);
        Matcher matcher = pattern.matcher(input);

        if (matcher.find()) {
            return input.substring(matcher.start(), matcher.end()).replaceAll("-", "");
        }

        return null;
    }

    /**
     * find an identifier for a release classifier
     * @param input the input string to parse (normally the branch name)
     * @return the release identifier, or null if nothing can be found
     */
    public String getReleaseIdentifierFromBranch(String input) {

        logger.info(String
                .format("try to find classifier from branch '%s'", input));

        if (input == null) {
            return null;
        }

        //find token beta in branch name like origin/beta/2.0.0
        Pattern pattern = Pattern.compile(releasePattern);
        Matcher matcher = pattern.matcher(input);
        if (matcher.matches()) {
            String classifier = matcher.group(2);
            logger.info(String
                    .format("found classifier '%s' based on branch %s", classifier, input));
            return classifier;
        }

        return null;
    }
}

package ch.inftec.ju.versioverride;

import org.apache.maven.model.Model;
import org.codehaus.plexus.logging.console.ConsoleLogger;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import java.io.IOException;

import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by rotscher on 24.09.15.
 */
public class ExternalStrategyTest {

    @Mock
    Model model = mock(Model.class);

    @Mock
    BuildServer buildServer = mock(BuildServer.class);


    @Before
    public void before() {
        VersionFactory.version = null;
    }

    @Test
    public void testWithBuildIdPropertySet() {

        System.setProperty("version.buildId", "true");

        try {

            when(buildServer.getBuildNumber()).thenReturn("101");

            Version version = VersionFactory.create("1.2.3-beta", model, buildServer, new ConsoleLogger());
            assertNotNull(version);
            assertEquals("1.2.3-beta-101", version.getOverriddenVersion());
        } catch (IOException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testWithoutBuildId() {

        System.setProperty("version.buildId", "false");

        try {

            when(buildServer.getBuildNumber()).thenReturn("101");

            Version version = VersionFactory.create("1.2.3-beta", model, buildServer, new ConsoleLogger());
            assertNotNull(version);
            assertEquals("1.2.3-beta", version.getOverriddenVersion());
        } catch (IOException e) {
            fail(e.getMessage());
        }
    }
}

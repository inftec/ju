package ch.inftec.ju.versioverride;

import org.apache.maven.model.Model;
import org.codehaus.plexus.logging.console.ConsoleLogger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by rotscher on 1/15/15.
 */

@RunWith(MockitoJUnitRunner.class)
public class GeneratedVersionTest extends AbstractVersionOverrideTest {

    @Mock
    Model model = mock(Model.class);

    @Mock
    BuildServer buildServer = mock(BuildServer.class);

    @Test
    public void testGetOverriddenVersionWithBuildNumberFromFile() throws Exception {
        try {
            when(model.getVersion()).thenReturn("5.8.1.4-RC");
            when(model.getGroupId()).thenReturn("ch.inftec.ju.ch.inftec.ju.versioverride.1");
            GeneratedVersion versionOverride = new GeneratedVersion(model, new BuildServerType.LocalBuildMachine(), new ConsoleLogger());
            assertEquals("5.8.1.4-RC-1", versionOverride.getOverriddenVersion());
        } catch (IOException e) {
            fail(e.getMessage());
        }
    }


    @Test
    public void testGetOverriddenVersionOnMasterBranch() throws Exception {

        VersionOverrideModelReader customVersionModelReader = new VersionOverrideModelReader();
        customVersionModelReader.setLogger(new ConsoleLogger());
        try {
            when(model.getVersion()).thenReturn("5.8.1.4-Final");
            when(model.getGroupId()).thenReturn("ch.inftec.ju.ch.inftec.ju.versioverride.1");
            when(buildServer.getBuildNumber()).thenReturn("44");
            when(buildServer.isOnMasterBranch()).thenReturn(true);

            GeneratedVersion versionOverride = new GeneratedVersion(model, buildServer, new ConsoleLogger());
            assertNotNull(model);
            assertEquals("5.8.1.4-44", versionOverride.getOverriddenVersion());
        } catch (IOException e) {
            fail(e.getMessage());
        }
    }
}

package ch.inftec.ju.versioverride;

import org.apache.maven.model.Model;
import org.codehaus.plexus.logging.console.ConsoleLogger;
import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

import static org.junit.Assert.*;

/**
 * This class tests versions which are generated and not provided as argument.
 */
public class GeneratedVersionModelReaderTest extends AbstractVersionOverrideTest {

    @Test
    public void testWithNoPropertySet() {
    	System.clearProperty(VersionOverrideModelReader.MAVENEXT_RELEASE_VERSION);
        InputStream correctPomFile = GeneratedVersionModelReaderTest.class.getResourceAsStream("valid/single/pom.xml");

        VersionOverrideModelReader customVersionModelReader = new VersionOverrideModelReader();
        customVersionModelReader.setLogger(new ConsoleLogger());
        try {
            Model model = customVersionModelReader.read(correctPomFile, new HashMap<String, Object>());
            assertNotNull(model);
            assertEquals("0.0.1-SNAPSHOT", model.getVersion());
        } catch (IOException e) {
            fail(e.getMessage());
        }
    }
    
    @Test
    @Ignore
    public void testWithEmptyPropertySet() {
        System.setProperty(VersionOverrideModelReader.MAVENEXT_RELEASE_VERSION, "");
        System.clearProperty("BUILD_NUMBER");
        InputStream correctPomFile = GeneratedVersionModelReaderTest.class.getResourceAsStream("valid/single/pom.xml");

        VersionOverrideModelReader customVersionModelReader = new VersionOverrideModelReader();
        customVersionModelReader.setLogger(new ConsoleLogger());

        try {
            Model model = customVersionModelReader.read(correctPomFile, new HashMap<String, Object>());
            assertNotNull(model);
            assertEquals("0.0.1-S-1", model.getVersion());
        } catch (IOException e) {
            fail(e.getMessage());
        }
    }
}
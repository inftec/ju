package ch.inftec.ju.versioverride;

import org.apache.maven.model.Model;
import org.codehaus.plexus.logging.console.ConsoleLogger;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import java.io.IOException;

import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by rotscher on 24.09.15.
 */
public class PomStrategyTest {

    @Mock
    Model model = mock(Model.class);

    @Mock
    BuildServer buildServer = mock(BuildServer.class);

    @Before
    public void before() {
        VersionFactory.version = null;
    }

    @Test
    public void testReleaseClassifierOnMasterBranch() {

        System.setProperty("version.override", "true");
        System.setProperty("classifier.type", "RELEASE");

        try {

            when(buildServer.getBuildNumber()).thenReturn("55");
            when(buildServer.getBranchName()).thenReturn("origin/master");
            when(buildServer.isOnMasterBranch()).thenReturn(true);
            when(model.getGroupId()).thenReturn("ch.inftec.ju.test");
            when(model.getVersion()).thenReturn("5.6.1-SNAPSHOT");

            Version version = VersionFactory.create(null, model, buildServer, new ConsoleLogger());
            assertNotNull(version);
            assertEquals("5.6.1-55", version.getOverriddenVersion());
        } catch (IOException e) {
            fail(e.getMessage());
        }
    }


    @Test
    public void testReleaseClassifierOnNonMasterBranch() {

        System.setProperty("version.override", "true");
        System.setProperty("classifier.type", "RELEASE");

        try {

            when(buildServer.getBuildNumber()).thenReturn("55");
            when(buildServer.isOnMasterBranch()).thenReturn(false);
            when(buildServer.getBranchName()).thenReturn("origin/rc/7.5.5");
            when(model.getGroupId()).thenReturn("ch.inftec.ju.test");
            when(model.getVersion()).thenReturn("5.6.1-SNAPSHOT");

            Version version = VersionFactory.create(null, model, buildServer, new ConsoleLogger());
            assertNotNull(version);
            assertEquals("5.6.1-rc-55", version.getOverriddenVersion());
        } catch (IOException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testDevelopClassifierOnDevelopBranch() {

        System.setProperty("version.override", "true");
        System.setProperty("classifier.type", "DEVELOP");

        try {

            when(buildServer.getBuildNumber()).thenReturn("33");
            when(buildServer.isOnDevelopBranch()).thenReturn(true);
            when(buildServer.getBranchName()).thenReturn("origin/develop");
            when(model.getGroupId()).thenReturn("ch.inftec.ju.test");
            when(model.getVersion()).thenReturn("6.4.1-SNAPSHOT");

            Version version = VersionFactory.create(null, model, buildServer, new ConsoleLogger());
            assertNotNull(version);
            assertEquals("6.4.1-S-33", version.getOverriddenVersion());
        } catch (IOException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testDevelopClassifierOnNonDevelopBranch() {

        System.setProperty("version.override", "true");
        System.setProperty("classifier.type", "DEVELOP");

        try {

            when(buildServer.getBuildNumber()).thenReturn("33");
            when(buildServer.isOnDevelopBranch()).thenReturn(false);
            when(buildServer.getBranchName()).thenReturn("origin/publish/FOOBAR-55_my_test_branch");
            when(model.getGroupId()).thenReturn("ch.inftec.ju.test");
            when(model.getVersion()).thenReturn("6.4.1-SNAPSHOT");

            Version version = VersionFactory.create(null, model, buildServer, new ConsoleLogger());
            assertNotNull(version);
            assertEquals("6.4.1-FOOBAR55-33", version.getOverriddenVersion());
        } catch (IOException e) {
            fail(e.getMessage());
        }
    }
}

package ch.inftec.ju.versioverride;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertTrue;

/**
 * Created by rotscher on 1/15/15.
 */

@RunWith(MockitoJUnitRunner.class)
public class AbstractBuildServerTest extends AbstractVersionOverrideTest {

    @Test
    public void testOnMasterBranch() throws Exception {
        BuildServer bamboo = new BuildServerType.BambooBuildServer() {
            @Override
            public String getBranchName() {
                return "origin/master";
            }
        };
        assertTrue(bamboo.isOnMasterBranch());
    }

    @Test
    public void testOnDevelopBranch() throws Exception {
        BuildServer bamboo = new BuildServerType.BambooBuildServer() {
            @Override
            public String getBranchName() {
                return "origin/develop";
            }
        };
        assertTrue(bamboo.isOnDevelopBranch());
    }

}

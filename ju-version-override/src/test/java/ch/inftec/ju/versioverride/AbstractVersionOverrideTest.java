package ch.inftec.ju.versioverride;

import org.apache.commons.io.IOUtils;
import org.junit.Before;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import static org.junit.Assert.fail;

/**
 * Created by rotscher on 1/24/15.
 */
public class AbstractVersionOverrideTest {
    @Before
    public void before() {
        System.clearProperty(VersionOverrideModelReader.MAVENEXT_CHECK_SNAPSHOT_DEP);
        System.clearProperty(VersionOverrideModelReader.MAVENEXT_CHECK_SNAPSHOT_DEP_FAIL_ON_ERROR);
        System.clearProperty(VersionOverrideModelReader.MAVENEXT_RELEASE_VERSION);
        System.clearProperty("buildserver.type");
        System.clearProperty("classifier.type");
		System.clearProperty("classifier.versionPrefix");
		System.clearProperty("classifier.manualPrefix");
        System.clearProperty("BUILD_NUMBER");
        System.clearProperty("bamboo_buildNumber");
        System.clearProperty("JOB_NAME");
        System.clearProperty("bamboo_buildKey");

        VersionOverrideModelReader.reset();

		try {
			FileOutputStream fos = new FileOutputStream(VersionOverrideModelReader.MAVENEXT_BUILDNUMBER_FILE);
			IOUtils.write("0", fos);
			IOUtils.closeQuietly(fos);
		} catch (FileNotFoundException e) {
			fail(e.getMessage());
		} catch (IOException e) {
			fail(e.getMessage());
		}
    }
}

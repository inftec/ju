package ch.inftec.ju.versioverride;

import org.apache.maven.model.Model;
import org.codehaus.plexus.logging.console.ConsoleLogger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by rotscher on 1/15/15.
 */

@RunWith(MockitoJUnitRunner.class)
public class JenkinsBuildServerTest extends AbstractVersionOverrideTest {

    @Mock
    Model model = mock(Model.class);

    @Test
    public void testGetBuildNumber() throws Exception {
        BuildServer jenkins = BuildServerType.JENKINS.getBuildServer();
        //buildnumber is defined as env variable in pom.xml, surefire plugin execution
        assertEquals("321", jenkins.getBuildNumber());
    }

    @Test
    public void testGetJobName() throws Exception {
        BuildServer jenkins = BuildServerType.JENKINS.getBuildServer();
        //jobname is defined as env variable in pom.xml, surefire plugin execution
        assertEquals("foobar_build", jenkins.getJobName());
    }

    @Test
    public void testDetectBuildServer() throws Exception {
        when(model.getVersion()).thenReturn("5.8.1.4-RC");
        when(model.getGroupId()).thenReturn("ch.inftec.ju.ch.inftec.ju.versioverride.1");
        assertEquals(BuildServerType.JENKINS, AbstractBuildServer.detectBuildServer(new ConsoleLogger()));
    }
}

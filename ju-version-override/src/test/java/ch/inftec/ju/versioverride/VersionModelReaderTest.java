package ch.inftec.ju.versioverride;

import org.apache.maven.MavenExecutionException;
import org.apache.maven.model.Dependency;
import org.apache.maven.model.Model;
import org.codehaus.plexus.logging.console.ConsoleLogger;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;

import static org.junit.Assert.*;

public class VersionModelReaderTest extends AbstractVersionOverrideTest {

    @Test
    public void testValidSinglePom() {

        InputStream correctPomFile = VersionModelReaderTest.class.getResourceAsStream("valid/single/pom.xml");

        VersionOverrideModelReader customVersionModelReader = new VersionOverrideModelReader();
        customVersionModelReader.setLogger(new ConsoleLogger());
        try {
            Model model = customVersionModelReader.read(correctPomFile, new HashMap<String, Object>());
            assertNotNull(model);
            assertEquals("0.0.1-SNAPSHOT", model.getVersion());
        } catch (IOException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testFailingSnapshotDep() {

        System.setProperty(VersionOverrideModelReader.MAVENEXT_RELEASE_VERSION, "1.2.3");
        System.setProperty(VersionOverrideModelReader.MAVENEXT_CHECK_SNAPSHOT_DEP, "TrUe");
        System.setProperty(VersionOverrideModelReader.MAVENEXT_CHECK_SNAPSHOT_DEP_FAIL_ON_ERROR, "TrUe");
        InputStream correctPomFile = VersionModelReaderTest.class.getResourceAsStream("valid/single/pom.xml");

        VersionOverrideModelReader customVersionModelReader = new VersionOverrideModelReader();
        customVersionModelReader.setLogger(new ConsoleLogger());
        try {
            customVersionModelReader.read(correctPomFile, new HashMap<String, Object>());
            fail("this test should fail with a illegal argument exception");
        } catch (IOException e) {
            if (e.getCause() instanceof MavenExecutionException) {
                assertTrue(e.getCause().getMessage().startsWith("there is a snapshot dependency"));
            } else {
                fail("expected MavenExecutionException");
            }
        }
    }

    @Test
    public void testValidMultiModulePom() {

        InputStream correctPomFile = VersionModelReaderTest.class.getResourceAsStream("valid/multi/modules/pom.xml");

        VersionOverrideModelReader customVersionModelReader = new VersionOverrideModelReader();
        customVersionModelReader.setLogger(new ConsoleLogger());
        try {
            Model model = customVersionModelReader.read(correctPomFile, new HashMap<String, Object>());
            assertNotNull(model);
            assertEquals("0.0.1-SNAPSHOT", model.getParent().getVersion());
        } catch (IOException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testValidMultiModulePomWithOverriddenVersion() {

        System.setProperty(VersionOverrideModelReader.MAVENEXT_RELEASE_VERSION, "1.2.3");
        System.setProperty(VersionOverrideModelReader.MAVENEXT_BE_STRICT, "false");
        InputStream correctPomFile = VersionModelReaderTest.class.getResourceAsStream("valid/multi/modules/project2/pom.xml");

        VersionOverrideModelReader customVersionModelReader = new VersionOverrideModelReader();
        customVersionModelReader.setLogger(new ConsoleLogger());
        try {
            Model model = customVersionModelReader.read(correctPomFile, new HashMap<String, Object>());
            assertNotNull(model);
        } catch (IOException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testNonFailingSnapshotDep() {
        System.setProperty(VersionOverrideModelReader.MAVENEXT_RELEASE_VERSION, "1.2.3");
        System.setProperty(VersionOverrideModelReader.MAVENEXT_CHECK_SNAPSHOT_DEP, "TrUe");
        // CustomVersionModelReader.MAVENEXT_CHECK_SNAPSHOT_DEP_FAIL_ON_ERROR, is false by default!!
        InputStream correctPomFile = VersionModelReaderTest.class.getResourceAsStream("valid/single/pom.xml");

        VersionOverrideModelReader customVersionModelReader = new VersionOverrideModelReader();
        customVersionModelReader.setLogger(new ConsoleLogger());
        try {
            Model model = customVersionModelReader.read(correctPomFile, new HashMap<String, Object>());
            assertNotNull(model);
            assertTrue(hasAtLeastOneSnapshotDependency(model.getDependencies()));
        } catch (IOException e) {
            fail("no exception should occur");
        }
    }

    @Test
    public void testNoSnapshotDep() {
        System.setProperty(VersionOverrideModelReader.MAVENEXT_RELEASE_VERSION, "1.2.3");
        System.setProperty(VersionOverrideModelReader.MAVENEXT_CHECK_SNAPSHOT_DEP, "TrUe");
        System.setProperty(VersionOverrideModelReader.MAVENEXT_CHECK_SNAPSHOT_DEP_FAIL_ON_ERROR, "TrUe");
        InputStream correctPomFile = VersionModelReaderTest.class.getResourceAsStream("no-snapshot-dep/pom.xml");

        VersionOverrideModelReader customVersionModelReader = new VersionOverrideModelReader();
        customVersionModelReader.setLogger(new ConsoleLogger());
        try {
            Model model = customVersionModelReader.read(correctPomFile, new HashMap<String, Object>());
            assertNotNull(model);
        } catch (IOException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testIsVersionOverridden() {
        System.setProperty(VersionOverrideModelReader.MAVENEXT_RELEASE_VERSION, "");
        assertTrue(new VersionOverrideModelReader().isVersionOverridden());

        System.clearProperty(VersionOverrideModelReader.MAVENEXT_RELEASE_VERSION);
        assertFalse(new VersionOverrideModelReader().isVersionOverridden());

    }


    /**
     * return
     * 
     * @param dependencies
     * @return
     */
    private boolean hasAtLeastOneSnapshotDependency(List<Dependency> dependencies) {
        for (Dependency dep : dependencies) {
            if (dep.getVersion().contains("SNAPSHOT")) {
                return true;
            }
        }
        return false;
    }

    private Model createAndInitializeDefaultVersionModelReader() {
        InputStream correctPomFile = VersionModelReaderTest.class.getResourceAsStream("no-snapshot-dep/pom.xml");

        VersionOverrideModelReader customVersionModelReader = new VersionOverrideModelReader();
        customVersionModelReader.setLogger(new ConsoleLogger());
        try {
            Model model = customVersionModelReader.read(correctPomFile, new HashMap<String, Object>());
            assertNotNull(model);
            return model;
        } catch (IOException e) {
            fail(e.getMessage());
        }

        return null;
    }
}
package ch.inftec.ju.versioverride;

import org.apache.maven.model.Model;
import org.codehaus.plexus.logging.console.ConsoleLogger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by rotscher on 1/15/15.
 */

@RunWith(MockitoJUnitRunner.class)
public class BambooBuildServerTest extends AbstractVersionOverrideTest {

    @Mock
    Model model = mock(Model.class);

    @Test
    public void testGetBuildNumber() throws Exception {
        BuildServer bamboo = BuildServerType.BAMBOO.getBuildServer();
        assertEquals("123", bamboo.getBuildNumber());
    }

    @Test
    public void testGetJobName() throws Exception {
        BuildServer bamboo = BuildServerType.BAMBOO.getBuildServer();
        //jobname is defined as env variable in pom.xml, surefire plugin execution
        assertEquals("bamboo-build", bamboo.getJobName());
    }

    @Test
    public void testDetectBuildServer() throws Exception {
        when(model.getVersion()).thenReturn("5.8.1.4-RC");
        when(model.getGroupId()).thenReturn("ch.inftec.ju.ch.inftec.ju.versioverride.1");
        assertEquals(BuildServerType.BAMBOO, AbstractBuildServer.detectBuildServer(new ConsoleLogger()));
    }

}

package ch.inftec.ju.versioverride;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by rotscher on 11.10.15.
 */
public class VersionStrategyTest {

    @Test
    public void testDetermine() throws Exception {
        VersionStrategy strategy = VersionStrategy.determine(null);
        assertNotNull(strategy);
        assertEquals(VersionStrategy.POM, strategy);

        strategy = VersionStrategy.determine("");
        assertNotNull(strategy);
        assertEquals(VersionStrategy.POM, strategy);

        strategy = VersionStrategy.determine("true");
        assertNotNull(strategy);
        assertEquals(VersionStrategy.POM, strategy);

        strategy = VersionStrategy.determine("false");
        assertNotNull(strategy);
        assertEquals(VersionStrategy.NONE, strategy);

        strategy = VersionStrategy.determine("1.2-rc-5");
        assertNotNull(strategy);
        assertEquals(VersionStrategy.EXTERNAL, strategy);
    }
}
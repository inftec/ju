package ch.inftec.ju.versioverride;

import org.codehaus.plexus.logging.console.ConsoleLogger;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;

/**
 * Created by rotscher on 17.09.15.
 */
public class BuildServerDetectTest {

    @Before
    public void before() {
        AbstractBuildServer.buildServerType = null;
    }

    @Test
    public void testDetectBuildServer() {
        System.setProperty("buildserver.type", BuildServerType.JENKINS.toString());
        BuildServerType type = AbstractBuildServer.detectBuildServer(new ConsoleLogger());
        assertEquals(BuildServerType.JENKINS, type);
    }
}

package ch.inftec.ju.versioverride;

import org.codehaus.plexus.logging.console.ConsoleLogger;
import org.junit.Test;
import org.mockito.Mock;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by rotscher on 1/23/15.
 */

public class VersionClassifierTest extends AbstractVersionOverrideTest {

    @Mock
    BuildServer buildServer = mock(BuildServer.class);

    @Test
    public void testGetDefaultProjectClassifier() throws Exception {
        VersionFactory.version = null;
        System.clearProperty("classifier.type");
        String snapshotResult = new VersionClassifier(new ConsoleLogger(), buildServer).getClassifier("SNAPSHOT");
        assertEquals("S", snapshotResult);

        String nonSnapshotResult = new VersionClassifier(new ConsoleLogger(), buildServer).getClassifier("RC");
        assertEquals("RC", nonSnapshotResult);
    }


    @Test
    public void testGetPersonalClassifier() throws Exception {
        System.setProperty("classifier.type", "PERSONAL");
        when(buildServer.getBranchName()).thenReturn("DEV-88_myfeature");
        when(buildServer.getJobName()).thenReturn("project-build");

        String snapshotResult = new VersionClassifier(new ConsoleLogger(), buildServer).getClassifier("SNAPSHOT");
        assertEquals("project-build_DEV88", snapshotResult);
    }

    @Test
    public void testGetPersonalClassifierWithNoIssueIdentifier() throws Exception {
        System.setProperty("classifier.type", "PERSONAL");
        when(buildServer.getBranchName()).thenReturn("a_dummy_branch");
        when(buildServer.getJobName()).thenReturn("project-build");

        String snapshotResult = new VersionClassifier(new ConsoleLogger(), buildServer).getClassifier("SNAPSHOT");
        assertEquals("project-build", snapshotResult);
    }

    @Test
    public void testGetIssueIdentifier() throws Exception {
        String result = new VersionClassifier(new ConsoleLogger(), buildServer).getIssueIdentifierFromBranch("feature/TEC-7-workshop-jee-best-practices");
        assertEquals("TEC7", result);

        result = new VersionClassifier(new ConsoleLogger(), buildServer).getIssueIdentifierFromBranch("feature/PR-6598994-workshop-jee-best-practices");
        assertEquals("PR6598994", result);

        result = new VersionClassifier(new ConsoleLogger(), buildServer).getIssueIdentifierFromBranch("feature/FOOBAR-06598994-workshop-jee-best-practices");
        assertEquals("FOOBAR06598994", result);
    }

    @Test
    public void testGetNoIssueIdentifier() throws Exception {
        String result = new VersionClassifier(new ConsoleLogger(), buildServer).getIssueIdentifierFromBranch("origin/master");
        assertNull(result);
    }

    @Test
    public void testGetReleaseClassifier() throws Exception {
        System.setProperty("classifier.type", "RELEASE");
        System.setProperty("buildserver.type", "UNKNOWN");
        String snapshotResult = new VersionClassifier(new ConsoleLogger(), buildServer).getClassifier("SNAPSHOT");
        assertEquals("S", snapshotResult);
    }

    @Test
    public void testGetReleaseClassifierOfNullBranch() throws Exception {
        System.setProperty("classifier.type", "RELEASE");
        System.setProperty("buildserver.type", "UNKNOWN");
        String snapshotResult = new VersionClassifier(new ConsoleLogger(), buildServer).getClassifier("foobar");
        assertEquals("foobar", snapshotResult);
    }

    @Test
    public void testGetBranchCategoryOfMaster() throws Exception {
        String result = new VersionClassifier(new ConsoleLogger(), buildServer).getReleaseIdentifierFromBranch("master/PR-55_abc");
        assertEquals("master", result);

        result = new VersionClassifier(new ConsoleLogger(), buildServer).getReleaseIdentifierFromBranch("origin/master/PR-55_abc");
        assertEquals("master", result);
    }

    @Test
    public void testGetBranchCategoryOfBetaBranch() throws Exception {
        String result = new VersionClassifier(new ConsoleLogger(), buildServer).getReleaseIdentifierFromBranch("beta/PR-88_a-branch");
        assertEquals("beta", result);
    }

    @Test
    public void testGetBranchCategoryOfBetaBranch2() throws Exception {
        String result = new VersionClassifier(new ConsoleLogger(), buildServer).getReleaseIdentifierFromBranch("origin/rc/PR-89_a-branch");
        assertEquals("rc", result);
    }

    @Test
    public void testDevelopClassifierNotOnDevelopBranch() throws Exception {
        VersionClassifier classifier = new VersionClassifier(new ConsoleLogger(), buildServer);
        System.setProperty("classifier.type", VersionClassifier.ClassifierType.DEVELOP.toString());
        when(buildServer.isOnDevelopBranch()).thenReturn(false);
        when(buildServer.isOnMasterBranch()).thenReturn(false);
        when(buildServer.getBranchName()).thenReturn("origin/DEV-553_a-feature-branch");
        assertEquals("DEV553", classifier.getClassifier("SNAPSHOT"));
    }


    @Test
    public void testDevelopClassifierNotOnDevelopBranchWithoutValidIssueId() throws Exception {
        VersionClassifier classifier = new VersionClassifier(new ConsoleLogger(), buildServer);
        System.setProperty("classifier.type", VersionClassifier.ClassifierType.DEVELOP.toString());
        when(buildServer.isOnDevelopBranch()).thenReturn(false);
        when(buildServer.isOnMasterBranch()).thenReturn(false);
        when(buildServer.getBranchName()).thenReturn("origin/invalidissue_id");
        assertEquals("S", classifier.getClassifier("SNAPSHOT"));
    }

    @Test
    public void testDevelopClassifierOnDevelopBranch() throws Exception {
        VersionClassifier classifier = new VersionClassifier(new ConsoleLogger(), buildServer);
        System.setProperty("classifier.type", VersionClassifier.ClassifierType.DEVELOP.toString());
        when(buildServer.isOnDevelopBranch()).thenReturn(true);
        when(buildServer.isOnMasterBranch()).thenReturn(false);
        when(buildServer.getBranchName()).thenReturn("origin/DEV-553_a-feature-branch");
        assertEquals("DEV553", classifier.getClassifier("SNAPSHOT"));
    }

    @Test
    public void testReleaseVersionClassifier() throws Exception {
        VersionClassifier classifier = new VersionClassifier(new ConsoleLogger(), buildServer);
        System.setProperty("classifier.type", VersionClassifier.ClassifierType.RELEASE.toString());
                when(buildServer.getBranchName()).thenReturn("origin/release/5.6.7");
        assertEquals("release", classifier.getClassifier("SNAPSHOT"));
    }
}

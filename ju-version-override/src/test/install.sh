#!/bin/bash

mvn_intallations=(~/appl/apache-maven-3.0.5 ~/appl/apache-maven-3.1.1 ~/appl/apache-maven-3.2.5)

for installation in ${mvn_intallations[@]} ; do
   cp target/ju-version-override*.jar ${installation}/lib/ext/version-override.jar
done

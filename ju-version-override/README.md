Version Override
======================

The motivation for all this is written down here http://diplingfh.blogspot.ch/2013/02/how-has-our-release-process-matured.html and https://docs.inftec.ch/display/TEC/version.override

Current release: 4.5.1 (Fall 2015)

## Installation

Get it from maven-central: http://search.maven.org/#search|ga|1|ch.inftec

- Install the ju-version-override.jar 
    - in _$M2_HOME/lib/ext_ or
    - (as of Maven 3.3.1): load the extension within the project (see below)
- Configure the install-custom-version-plugin in your pom (see below)

## Usage

There are two modus operandi: either the version is injected from outside, or the version is generated based on the pom's project version.

### Injection from outside

    mvn clean install -Dversion.override=1.2.3-RC [version.buildId=false]                    
activate version override and provide a version, buildId is appended by default

### Generated based on pom.xml

When generating the classifier of a version, three use cases are distinguished:

1. build on master branch, which means, no classifier is used at all
2. build on develop branch, the classifier is set to "S"
3. build on any other branch, the classifier is generated based on the property _-Dclassifier.type_

When generating the buildId, the environment is taken into account:

1. building locally, the buildnumber is kept in a file named _.buildnumber_
2. building on a CI server (Jenkins or Bamboo) the buildId is taken from the jobs build run (e.g. BUILD_NUMBER on Jenkins)

The buildId is always appended, unless it is disabled with _-Dversion.buildId=false_

#### Examples

##### Build on local dev machines

    mvn clean install -Dversion.override

##### Official final or pre releases built on CI server

    mvn clean install -Dversion.override -Dclassifier.type=RELEASE                    

version's classifier (e.g. SNAPSHOT) is overridden, the classifier is generated based on the current branch: _origin/subbranch/branchname_ the classifier is "subbranch" (e.g. _origin/beta/2.0.0_, classifier is _beta_).

##### Official development releases built on CI server

    mvn clean install -Dversion.override -Dclassifier.type=DEVELOP
                        
version's classifier (e.g. SNAPSHOT) is overridden, the classifier is generated based on the current branch: _origin/publish/ISSUE-533_ the classifier is ISSUE533.

##### Development feature releases built on CI server

    mvn clean install -Dversion.override -Dclassifier.type=PERSONAL (deprecated)                    
with this option the version's classifier (e.g. SNAPSHOT) is overridden: executed on a CI server (JENKINS or BAMBOO) the classifier is composed of the job's name and the issue pattern of JIRA (e.g. FOO-1502). Running locally, then "local" is taken.

*This option should be used for local builds and for building "personal" feature branches which are used for some kind of test deployments.*

### Other options

    -Dversion.override.strict=false                    
only override versions of modules with the same groupId AND the same ${project.version} like the root/parent module. This avoids version overriding of modules not in the same reactor but having the same groupId. Default is set to _true_

    -Dversion.override.check-snapshot-dependency=true                    
checks if there are still SNAPSHOT dependencies (this happens when the version of "internal" dependencies is not set to ${project.version}). Default is _false_

    -Dbuildserver.type=[UNKNOWN|BAMBOO|JENKINS]               
override the auto-detection of the build server

### Hints
  * version.override best works with version formats x.y.z-CLASSIFIER, e.g. 0.5.1-SNAPSHOT or 1.0-RC (delim in tokenizer: dash)
  * When the generated classifier is "SNAPSHOT", the literal is shorten to "S", e.g. 0.1.0-SNAPSHOT becomes 0.1.0-S-10
  * IMPORTANT: specify the version of internal dependencies with ${project.version}!
  * version.override has been tested on the following build environments: JENKINS, BAMBOO and on local developer machines
  * version.override is used along with Git, all other version controls has not been tested (but should more or less work there, too)

## Releases

### Release 4.5.1

* Injected version (-Dversion.override=1.0-RC) is appended with the buildId, result is e.g. 1.0-RC-5
* Refactoring of existing code

this release has been tested with  

* JDK 1.8
* Maven 3.3.x

### Release 4.4.x

* Bamboo integration
* Personal version classifiers
* new classifier types: RELEASE, DEVELOP

### Release 0.3.3

* fix for strict option (-Dversion.override.strict)

### Release 0.3.2

* removed dependecy to commons-io (this must not be installed anymore)

### Release 0.3.1

* install-custom-version-plugin for replacing the overridden version in the pom.xml which gets installed  into the local and deployed to the remote repository

this release has been tested with  

* JDK 1.6/1.7
* Maven 3.0.3, 3.0.4, 3.0.5, 3.1.0

### Release 0.2.x

* integrated a build number generation algorithm in the version.override feature

### Release 0.1.1 (November 2012)

* override _${project.version}_ on the fly
    to activate that feature execute: _mvn clean install -Dversion.override=1.2.3-RC-5_


## About version and ${project.version}

The version.override feature is based on the version which is defined in _${project.version}_ in pom.xml. Normally the version is set to something like 1.1.0-SNAPSHOT or 2.0.5-RC.

version.override extends this static version: With each build an unique version is automatically generated, according to the following pattern:

VERSIONSTRING-CLASSIFIER-BUILDID

_VERSIONSTRING_ is the main version defined in _${project.version}_. Normally defined according to major.minor[.build[.revision]]
    pattern. This value is never changed by version.override.

_CLASSIFER_ the classifier denotes the development phase of a build and is generated at build time based on the branch which is built. This means in the pom.xml the classifier always remains SNAPSHOT.

_BUILDID_ is a unique id within the same buildstream (e.g. two sequential execution of the same build job should produce two different build ids)

### Examples
_${project.version}_ is 4.4-SNAPSHOT

* RELEASE): 4.4-5 (on master), 4.4-beta-8 (on origin/beta/4.4), 4.4-rc-1 (on origin/rc/4.4)
* DEVELOP): 4.4-S-15 (on develop), 4.4-PR32-5 (on origin/publish/PR-32-a_branch_name)

## configure maven extension within project

This is an alternative to the installation of the extension into _$M2_HOME/lib/ext_

Create a folder and a file: _.mvn/extensions.xml_

    <?xml version="1.0" encoding="UTF-8"?>
    <extensions>
        <extension>
            <groupId>ch.inftec.ju</groupId>
            <artifactId>ju-version-override</artifactId>
            <version>4.5.1-X</version>
        </extension>
    </extensions>

## configure install-custom-version-plugin

Here is an example configuration to put into the root pom.xml

    <profiles>
        <profile>
            <id>version.override</id>
            <activation>
                <property>
                    <name>version.override</name>
                </property>
            </activation>
            <build>
                <plugins>
                    <!-- configure the custom install plugin for lightweight release builds -->
                    <plugin>
                        <groupId>ch.rotscher.maven.plugins</groupId>
                        <artifactId>install-custom-version-plugin</artifactId>
                        <version>0.3.4-pre-3</version>
                        <executions>
                            <execution>
                                <id>version-override</id>
                                <phase>package</phase>
                                <goals>
                                    <goal>version-override</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>
                </plugins>
            </build>
        </profile>
    </profiles>
